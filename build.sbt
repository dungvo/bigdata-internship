val sparkCore = "org.apache.spark" % "spark-core_2.11" % "2.2.0" % "provided"
val sparkSQL = "org.apache.spark" % "spark-sql_2.11" % "2.2.0" % "provided"
val redis = "RedisLabs" % "spark-redis" % "0.3.2" % "provided"
val redisClient = "net.debasishg" %% "redisclient" % "3.4" % "provided"

lazy val root = (project in file(".")).
  settings(
    name := "task6",
    version := "1.0",
    scalaVersion := "2.11.12",
    mainClass in Compile := Some("Task6")
  )

libraryDependencies ++= Seq(sparkCore, sparkSQL, redis, redisClient)
// META-INF discarding
assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}
