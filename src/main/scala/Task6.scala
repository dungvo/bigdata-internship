import com.redis.RedisClient
import org.apache.spark.api.java.function.MapPartitionsFunction
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql._
import org.spark_project.guava.net

import scala.io.Source

object Task6 {
  var redisHost = ""
  var redisPort = 0
  var date = ""

  def getSecond(domain: String): String = {
    try{
      return net.InternetDomainName.from(domain).topPrivateDomain().name()
    }
    catch {
      case e:Exception => return domain
    }
  }

  def getTop(domain: String): String = {
    val filename = "effective_tld_names.dat"
    var top = ""
    val second = getSecond(domain)
    if (second == null){
      return "null"
    }
    val in = this.getClass.getResourceAsStream(filename)
    for (lineir <- Source.fromInputStream(in).getLines) {
      var line = lineir
      if (line.length > 0){
        if (line(0) == '*'){
          line = line.replace("*.", "")
        }
        try {
          if (line(0) != '/' && second.contains("." + line) && line.length > top.length){
            top = line
          }
        }
        catch {
          case e:Exception => top = "null"
        }
      }
    }
    return top
  }

  def loadCategory(path: String): Unit = {
    val r = new RedisClient(redisHost, redisPort)
    val sQLContext = SparkSession.builder().getOrCreate()
    val df = sQLContext.read.format("csv")
      .option("delimiter", "\t")
      .load(path)
    val arr = df.collect()
    for (i <- arr) {
      try{
        r.set(i(0), i(1))
      }
      catch {
        case e:Exception => println("Error category: " + i)
      }
    }
  }

  def loadContract(path: String): Unit = {
    val r = new RedisClient(redisHost, redisPort)
    val sQLContext = SparkSession.builder().getOrCreate()
    val df = sQLContext.read.format("csv")
      .option("delimiter", "\t")
      .load(path)
    val arr = df.collect()
    for (i <- arr) {
      try{
        r.lpush(i(0), i(3))
        r.lpush(i(0), i(2))
        r.lpush(i(0), i(1))
      }
      catch {
        case e:Exception => println("Error contract: " + i)
      }
    }
  }

  def ranking(df: DataFrame, sparkContext: SparkContext): Unit = {
    val sQLContext = SparkSession.builder().getOrCreate()
    import sQLContext.implicits._
    
    val dfr = df.mapPartitions( iterator => {
      val r = new RedisClient(redisHost, redisPort)
      iterator.map(p => {
        val domain = p.getString(9)
        val second = getSecond(domain)
        val client = p.getString(25)
        var cate = "NA"
        try{
          if (r.exists(domain)){
            cate = r.get(domain).get
          }
        }
        catch {
          case e:Exception => println(e.toString + " error domain: " + domain)
        }
        (second, cate, client)
      })
    }).toDF("second", "category", "client")
    

    ////    Số query của 1 second domain
    val dfrq = dfr.groupBy("second").count()
      .toDF("second", "query-count")
    ////    Số client của 1 second domain
    val dfrc = dfr.distinct().groupBy("second").count()
      .toDF("second", "client-count")

    val ranking = dfr.join(dfrq, "second").join(dfrc, "second")
      .map(p => {
        var query:Long = 0
        var client:Long = 1
        try {
          query = p.getLong(3)
          client = p.getLong(4)
        }
        catch {
          case e:Exception => println(e.toString + " query " + p.getString(3) + " client " + p.getString(4))
        }

        val qpc = (query.toFloat/client.toFloat).formatted("%.3f")
        (p.getString(0), p.getString(1), p.getString(2), qpc,
          p.getLong(4))
      })
      .toDF("second", "category", "client", "query-per-client", "client-count")
      .drop("client").distinct()
      .withColumn("rank", org.apache.spark.sql.functions.rank()
        .over(org.apache.spark.sql.expressions.Window.orderBy($"client-count".desc)))

    val sum = sparkContext.broadcast(dfr.count())
    //     Phần trăm query
    val per = dfrq.map(p => {
      (p.getString(0), (p.getLong(1).toFloat / sum.value))
    }).toDF("second", "query-percent")

    val reorder =  Array("rank", "second", "category", "query-per-client", "client-count", "query-percent")
    ranking.join(per, "second").select(reorder.head, reorder.tail: _*)
      .write.mode(SaveMode.Overwrite)
      .format("csv")
      .save("out/" + date + "/ranking")

    //    Rank by category
    val dfc = dfr.join(dfrc, "second").drop("client")  //second category  client-count
    val cats = dfc.select("category").distinct().collect()
    for (cat <- cats){
      println(cat(0))
      val catRank = dfc.filter($"category" === cat(0)).distinct()
        .withColumn("rank", org.apache.spark.sql.functions.rank()
          .over(org.apache.spark.sql.expressions.Window.orderBy($"client-count".desc)))
      catRank.drop("client-count").write.mode(SaveMode.Overwrite)
        .format("csv")
        .save("out/" + date + "/rankbycategory/" + cat(0))
    }
  }

  def queryPerMalware(dfBlack: Dataset[Row]) = {
    val sQLContext = SparkSession.builder().getOrCreate()
    import sQLContext.implicits._
    dfBlack.groupBy($"_c24").count().toDF("malware", "query")
      .write.mode(SaveMode.Overwrite).format("csv")
      .save("out/" + date + "/query-per-malware")
  }

  def queryPerDomain(df: DataFrame) = {
    val sQLContext = SparkSession.builder().getOrCreate()
    import sQLContext.implicits._
    df.groupBy($"_c9").count().toDF("domain", "query")
      .write.mode(SaveMode.Overwrite).format("csv")
      .save("out/" + date + "/query-per-domain")
  }

  def queryPerSeCond(df: DataFrame) = {
    val sQLContext = SparkSession.builder().getOrCreate()
    import sQLContext.implicits._
    df.map(p => (getSecond(p.getString(9)))).groupBy($"value")
      .count().write.mode(SaveMode.Overwrite).format("csv")
      .save("out/" + date + "/query-per-second")
  }

  def clientPerDomain(df: DataFrame) = {
    val sQLContext = SparkSession.builder().getOrCreate()
    import sQLContext.implicits._
    df.groupBy($"_c9", $"_c25").count()
      .groupBy($"_c9").count()
      .write.mode(SaveMode.Overwrite).format("csv")
      .save("out/" + date + "/client-per-domain")
  }

  def clientPerSecond(df: DataFrame) = {
    val sQLContext = SparkSession.builder().getOrCreate()
    import sQLContext.implicits._
    df.map(p => (getSecond(p.getString(9)), p.getString(25)))
      .groupBy($"_1", $"_2").count()
      .groupBy($"_1").count()
      .write.mode(SaveMode.Overwrite).format("csv")
      .save("out/" + date + "/client-per-second")
  }

  def clientPerMalware(dfBlack: Dataset[Row]) = {
    val sQLContext = SparkSession.builder().getOrCreate()
    import sQLContext.implicits._
    dfBlack.groupBy($"_c24", $"_c25").count()
      .groupBy($"_c24").count()
      .write.mode(SaveMode.Overwrite).format("csv")
      .save("out/" + date + "/client-per-malware")
  }

  def malwareCount(dfBlack: Dataset[Row], sparkContext: SparkContext) = {
    val sQLContext = SparkSession.builder().getOrCreate()
    import sQLContext.implicits._
    val malcount1 = dfBlack.count()
    val malcount2 = dfBlack.groupBy($"_c24").count().count()
    val file = sparkContext.parallelize(Seq("Count malware: " + malcount1,
      "Count malware types: " + malcount2)).toDF()
        .write.format("csv")
        .mode(SaveMode.Overwrite)
        .save("out/" + date + "/malware-count")
  }

  def domainPerTld(df: DataFrame) = {
    val sQLContext = SparkSession.builder().getOrCreate()
    import sQLContext.implicits._
    df.map(p => {
      (getTop(p.getString(9)), p.getString(9))
    }).groupBy($"_1").count()
      .write.format("csv")
      .mode(SaveMode.Overwrite)
      .save("out/" + date + "/domain-per-tld")
  }

  def clientPerTld(df: DataFrame) = {
    val sQLContext = SparkSession.builder().getOrCreate()
    import sQLContext.implicits._
    df.map(p => {
      (getTop(p.getString(9)), p.getString(25))
    }).groupBy($"_1").count()
      .toDF()
      .write.format("csv")
      .mode(SaveMode.Overwrite)
      .save("out/" + date + "/client-per-tld")
  }

  def querydomainPerCategory(df: DataFrame) = {
    val sQLContext = SparkSession.builder().getOrCreate()
    import sQLContext.implicits._
    
    val dfq = df.mapPartitions(iterator => {
      val r= new RedisClient(redisHost, redisPort)
      iterator.map(p => {
        val domain = p.getString(9)
        var cate = "NA"
        try{
          if (r.exists(domain)){
            cate = r.get(domain).get
          }
        }
        catch {
          case e:Exception => println("error domain: " + domain)
        }
        (cate, domain)
      })
    }).persist()
    
    dfq.groupBy("_1").count()
      .write.mode(SaveMode.Overwrite).format("csv")
      .save("out/" + date + "/query-per-category")

    //    Số lượng domain trên từng category
    dfq.distinct().groupBy("_1").count()
      .write.mode(SaveMode.Overwrite).format("csv")
      .save("out/" + date + "/domain-per-category")
  }

  def clientPerCategory(df: DataFrame) = {
    val sQLContext = SparkSession.builder().getOrCreate()
    import sQLContext.implicits._
    df.mapPartitions(iterator => {
      val r= new RedisClient(redisHost, redisPort)
      iterator.map(p => {
        val domain = p.getString(9)
        val client = p.getString(25)
        var cate = "NA"
        try{
          if (r.exists(domain)){
            cate = r.get(domain).get
          }
        }
        catch {
          case e:Exception => println("error domain: " + domain)
        }
        (cate, client)
      })
    }).distinct().groupBy("_1").count()
      .write.mode(SaveMode.Overwrite).format("csv")
      .save("out/" + date + "/client-per-category")
  }

  def queryclientPerProvince(df: DataFrame) = {
    val sQLContext = SparkSession.builder().getOrCreate()
    import sQLContext.implicits._
    val dfp = df.mapPartitions(iterator => {
      val r= new RedisClient(redisHost, redisPort)
      iterator.map(p => {
        val client = p.getString(25)
        var province = "NA"
        val domain = p.getString(9)
        try{
          if (r.llen(client).get != 0){
            province = r.lindex(client, 1).get
          }
        }
        catch {
          case e:Exception => println(e.toString + " error client: " + client)
        }
        (domain, province, client)
      })
    }).toDF("domain", "province", "client").persist()

    //      Số lượng query theo tỉnh thành
    dfp.drop("client").groupBy("province").count()
      .write.mode(SaveMode.Overwrite).format("csv")
      .save("out/" + date + "/query-per-province")

    //      Số lượng client theo tỉnh thành
    dfp.drop("client").distinct().groupBy("province").count()
      .write.mode(SaveMode.Overwrite).format("csv")
      .save("out/" + date + "/client-per-province")

    //    Số lượng client theo từng tỉnh thành của từng domain
    dfp.groupBy("domain", "province").count()
      .write.mode(SaveMode.Overwrite).format("csv")
      .save("out/" + date + "/client-of-domain-per-province")
  }

  def processByDate(path: String, sparkContext: SparkContext): Unit = {
    val sQLContext = SparkSession.builder().getOrCreate()
    import sQLContext.implicits._

    val df = sQLContext.read.format("csv")
      .option("delimiter", "\t")
      .load(path)
      .persist()

    val dfBlack = df.filter($"label" === "black").persist()

//    Số lượng query của từng malware
    queryPerMalware(dfBlack)

//    Số lượng query của từng domain
    queryPerDomain(df)

//    Số lượng query của từng second
    queryPerSeCond(df)

//    Số client unique của domain
    clientPerDomain(df)

//    Số client unique của second
    clientPerSecond(df)

//    Số client unique của malware
    clientPerMalware(dfBlack)

//    Số lượng malware theo ngày
    malwareCount(dfBlack, sparkContext)

//    Số domain của từng Top-Level-domain
    domainPerTld(df)

//    Số lượng client trên từng TOP-LEVEL-DOMAIN
    clientPerTld(df)

//    Số lượng query/domain trên từng category
    querydomainPerCategory(df)

//      Số lượng client trên category
    clientPerCategory(df)

//    Số lượng query/client theo tỉnh thành
    queryclientPerProvince(df)

//    Ranking
    ranking(df, sparkContext)

  }

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("yarn").setAppName("defaultAppName")
    val sparkContext = new SparkContext(conf)
    redisHost = args(1)
    redisPort = args(2).toInt
    date = args(0).split("day=")(0)
    processByDate(args(0), sparkContext)
  }
}
